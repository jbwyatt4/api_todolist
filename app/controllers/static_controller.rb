require 'rails/application_controller'

class StaticController < Rails::ApplicationController
  layout false

  def index
    render json: {
      status: 400,
      errors: ["Bad URL"]
    }.to_json
  end
end
