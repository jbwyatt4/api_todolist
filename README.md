# README

Part of my Rails/ReactJS portfolio. Requires Rails 5.2 and npm.

Client side/Reactjs code in /client folder

## GETTING STARTED

cd apt_test

rails s -p 3001

cd client

yarn start or npm run start

or (if you haven't used it in a while-runs git pull)

./run.sh

or (for quick starts during dev)

./startdev.sh

### Running the API

Requires httpie package

http :3001/todos

http POST :3001/todos title=Mozart created_by=1

http PUT :3001/todos/1 title=Beethoven

http DELETE :3001/todos/1

### Run tests

bundle exec rspec

or

./test.sh

## PRODUCTION
If deploying to heroku:

heroku buildpacks:clear

heroku buildpacks:set heroku/nodejs

heroku buildpacks:add heroku/ruby --index 2

## Misc Notes
Created with:

create-repack-app apt_test

if you get a pg error change config/database.yml as well as gemfile

Based on code from:

https://scotch.io/tutorials/build-a-restful-json-api-with-rails-5-part-one

### License
Copyright (C) 2017-2018 John B. Wyatt IV

AGPL v3 or higher.

See license text at:

https://spdx.org/licenses/AGPL-3.0-or-later.html
